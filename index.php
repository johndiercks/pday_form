<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Portfolio Day Form</title>
    <script src="index.js"></script>

    <link rel="stylesheet" href="assets\css\main.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <h1 class="col-md-2 col-md-offset-5">Portfolio Day</h1>

            <form action="/my-handling-form-page" method="post">
               
                <div class="col-md-2 col-md-offset-5">
                    <h4>First Name</h4>
                    <input type="text" id="name" name="user_name">
                </div>
                <div class="col-md-2 col-md-offset-5">
                    <h4>Last Name</h4>
                    <input type="text" id="name" name="user_name">
                </div>
                <div class="col-md-2 col-md-offset-5">
                    <h4>Program</h4>
                    <input type="text" id="name" name="user_name">
                </div>
                <div class="col-md-2 col-md-offset-5">
                    <h4>Secondary Program</h4>
                    <input type="text" id="name" name="user_name">
                </div>
                <div class="col-md-2 col-md-offset-5">
                    <h4>Website Address</h4>
                    <input type="text" id="name" name="user_name">
                </div>
                <div class="col-md-2 col-md-offset-5">
                    <h4>Secondary Website</h4>
                    <input type="text" id="name" name="user_name">
                </div>
                <div class="col-md-2 col-md-offset-5">
                    <h4>Personal Email</h4>
                    <input type="text" id="name" name="user_name">
                </div>
                <div class="col-md-2 col-md-offset-5">
                    <h4>Hometown</h4>
                    <input type="text" id="name" name="user_name">
                </div>
                <div>
                    <h4>Career Goals</h4>
                    <input type="text" id="name" name="user_name">
                </div>
                <div class="col-md-2 col-md-offset-5">
                    <h4>Hobbies</h4>
                    <input type="text" id="name" name="user_name">
                </div>
            
            </form>
        </div>
    </div>
</body>
</html>